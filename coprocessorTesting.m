clc;
clear all; % borra el workspace

N=1024;  % define el numero de elementos de cada vector

% for i = 1:100

%Genera vectores A y B de 1024 elementos con numeros positivos 
%(puede adaptarse facilmente si usan negativos y positivos).
A=ceil(rand(N,1)*254);
B=ceil(rand(N,1)*254);

%Guarda vectores A y B (cada uno de una columna de 1024 filas) en un
%archivo de texto. Cada linea del archivo contiene un elemento.
h= fopen('VectorA.txt', 'w');
fprintf(h, '%i\n', A);
fclose(h);

h= fopen('VectorB.txt', 'w');
fprintf(h, '%i\n', B);
fclose(h);

% Calcula valores de referencia para las operaciones, realizadas en forma local en el host
sumVec_host = A+B;
avgVec_host = (A+B)/2;
man_host = sum(abs(A-B));

%% A partir de aca se realizan las operaciones por medio de comandos al coprocesador

% Primero setear puerto serial
COM_port = "COM4";

% Baudios de conexion UART esta fijo en 115200

% Los siguientes comandos son con formato tentativo. 
% Puede aplicar cambios menores para adaptarlos a su implementacion, lo cual debe quedar claramente documentado.
% En cualquier caso, debe incluir solo argumentos necesarios para cada operacion. 
% No aplique aca "parches de software" para cubrir deficiencias en el dise�o de hardware.
% No se aceptar�n comentarios del tipo: "hay que poner ese argumento porque sino no funciona", sin una justificacion adecuada.

%% writeVec escribe un vector almacenado en un archivo de texto en la BRAM indicada por medio de la UART
write2dev('vectorB.txt','BRAMB',COM_port); 
write2dev('vectorA.txt','BRAMA',COM_port); 

%% readVec lee el contenido de la BRAM indicada por medio de la UART
 VecA_device = command2dev('readVec', 'BRAMA', COM_port);


%%  
 VecB_device = command2dev('readVec', 'BRAMB', COM_port);

%% A
 sumVec_device = command2dev('sumVec', COM_port); %realiza la suma elemento a elemento de los vectores almacenados y envia el resultado por la UART

 %%
 avgVec_device = command2dev('avgVec', COM_port);

 %% 
 man_device = command2dev('manDist', COM_port); %realiza el calculo de la distancia de Manhattan entre dos vectores y envia el resultado por la UART
 
%% Validacion.
% Los resultados _diff deberian ser 0 (o cercanos, dependiendo de su
% decision de diseno en el diseno del coprocesador). Si no es 0, indique
% claramente por que en su informe.

sumVec_diff = sum(sumVec_host - sumVec_device);
avgVec_diff = sum(avgVec_host - avgVec_device);
man_diff = man_host - man_device;

% end

%% Funciones
function write2dev(filename,bank_memmory,port)
    %abrir puerto serial
    device = serialport(port,115200);
    
    %mandar instruccion
    write(device,"WRITE  ","string");
    
    %mandar instruccion
    write(device,bank_memmory,"string");
    
    %crear arreglo de datos y mandarlo
    [data,delimiterOut]=importdata(filename);
    for i = 1:1024
        write(device,data(i),"uint8");
%         pause(0.001);
    end

    clear device;
end

function out = command2dev( varargin )
   
    if length(varargin)>2
        
        %abrir puerto serial
        port = varargin{3};
        device = serialport(port,115200);
    
        %mandar instruccion
        cmd = varargin{1};
        while length(cmd) < 7
            cmd = cmd + " " ;
        end
        write(device, cmd,  "string" ) ;
        
        %mandar FLAG
        write(device, varargin{2} , "string" ) ;
  
    else
        %abrir puerto serial
        port = varargin{2};
        device = serialport( port, 115200);
        %mandar instruccion
        cmd = varargin{1};
        while strlength(cmd) < 7
            cmd = cmd + " " ;
        end
        write(device,cmd,"string") ;
    end
    
        %lee la respuesta de la operacion
    if ( varargin{1} == "manDist" ) || ( varargin{1} == "eucDist" ) 
        Bytes = read(device,4,"uint8");
        Bytes = Bytes.' ;
        out = Bytes(4)*2^24 + Bytes(3)*2^16 + Bytes(2)*(2)^8 + Bytes(1)*(2)^0;
        %out = Bytes(1)*2^24 + Bytes(2)*2^16 + Bytes(3)*(2)^8 + Bytes(4)*(2)^0;

    else 
        Bytes = read(device,1024,"uint8");
        out = Bytes.';
    end 

    clear device;
end
