`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 22.11.2022 11:31:25
// Design Name: 
// Module Name: uart_rx_demux
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module uart_rx_demux(
    input  logic           clk, rst,
           logic [3:0]     dmx_ctrl,
           logic [7:0]     rx_data,
    output logic [7*8-1:0] cmd_string,
           logic [5*8-1:0] flag_string,
           logic [7:0]     num
    );
    
    logic [7*8-1:0] cmd_string_next;
    logic [5*8-1:0] flag_string_next;
    logic [7:0]     num_next;
    
    always_ff @(posedge clk) begin
		if (rst) begin //rst might be not needed here
			cmd_string  <= 0;
			flag_string <= 0;
			num         <= 0;
		end
		else begin
			cmd_string  <= cmd_string_next;
			flag_string <= flag_string_next;
			num         <= num_next;
		end
    end 
    
    always_comb begin
        cmd_string_next = cmd_string;
        flag_string_next = flag_string;
        num_next = num;
//        case(dmx_ctrl)
//            'd1: cmd_string_next[7:0]   = rx_data;
//            'd2: cmd_string_next[15:8]  = rx_data;
//            'd3: cmd_string_next[23:16] = rx_data;
//            'd4: cmd_string_next[31:24] = rx_data;
//            'd5: cmd_string_next[39:32] = rx_data;
//            'd6: cmd_string_next[47:40] = rx_data;
//            'd7: cmd_string_next[55:48] = rx_data;
//            'd8: flag_string_next[7:0]   = rx_data;
//            'd9: flag_string_next[15:8]  = rx_data;
//            'd10: flag_string_next[23:16] = rx_data;
//            'd11: flag_string_next[31:24] = rx_data;
//            'd12: flag_string_next[39:32] = rx_data;
//            'd13: num_next = rx_data;   
//        endcase
        case(dmx_ctrl)
        // comandos
            'd1: cmd_string_next[55:48]   = rx_data;
            'd2: cmd_string_next[47:40]  = rx_data;
            'd3: cmd_string_next[39:32] = rx_data;
            'd4: cmd_string_next[31:24] = rx_data;
            'd5: cmd_string_next[23:16] = rx_data;
            'd6: cmd_string_next[15:8] = rx_data;
            'd7: cmd_string_next[7:0] = rx_data;
         // flags
            'd8: flag_string_next[39:32]   = rx_data;
            'd9: flag_string_next[31:24]  = rx_data;
            'd10: flag_string_next[23:16] = rx_data;
            'd11: flag_string_next[15:8] = rx_data;
            'd12: flag_string_next[7:0] = rx_data;
         // numero
            'd13: num_next = rx_data;   
        endcase
    end
    
endmodule
