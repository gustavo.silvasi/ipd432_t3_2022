`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11.09.2022 14:57:38
// Design Name: 
// Module Name: BCD
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module BCD(
    input logic clk,
    input logic [6:0] BCD_in,
    input logic state,
    output logic [6:0] sevenSeg
    );
    always_ff @(posedge clk) begin
//    always_comb begin
        if(state==1)
            case(BCD_in)
//                "A":    sevenSeg <= ~7'b1110111;
//                "G":    sevenSeg <= ~7'b1101111;
//                "I":    sevenSeg <= ~7'b0110000;
//                "S":    sevenSeg <= ~7'b1101101;
//                "T":    sevenSeg <= ~7'b1111000;
//                "U":    sevenSeg <= ~7'b0111110;
//                "V":    sevenSeg <= ~7'b0111110;
//                "O":    sevenSeg <= ~7'b0111111;
//                "L":    sevenSeg <= ~7'b0111000;
//                ":":     sevenSeg <= ~7'b1001000;
                'd0:     sevenSeg <= ~7'b0111111;
                'd1:     sevenSeg <= ~7'b0000110;
                'd2:     sevenSeg <= ~7'b1011011;
                'd3:     sevenSeg <= ~7'b1001111;
                'd4:     sevenSeg <= ~7'b1100110;
                'd5:     sevenSeg <= ~7'b1101101;
                'd6:     sevenSeg <= ~7'b1111101;
                'd7:     sevenSeg <= ~7'b0000111;
                'd8:     sevenSeg <= ~7'b1111111;
                'd9:     sevenSeg <= ~7'b1100111;
                default: sevenSeg <= ~7'b0000000;
            endcase
        else begin
            sevenSeg = ~7'b0000000;
        end
    end        
endmodule
