`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 23.12.2022 16:05:19
// Design Name: 
// Module Name: Processing_Core_2
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Processing_Core_2 #(parameter word='d8, parameter num_word='d1024) (
    input  logic [num_word*word-1:0]   outA,
           logic [num_word*word-1:0]   outB,
           logic [2:0]     opCode,
           logic           clk, rst,
           logic           tx_ready,
           logic           op_start, 
    output logic [num_word*word-1:0]   result_out,
           logic eUART, eDisp 
    );
    
    // FALTA TODO EL MODULO
    PC_Controller_2 PC_ctrl ( .clk(clk), .rst(rst), .tx_ready(tx_ready) ,
                              .op_start(op_start), .opCode(opCode) ,
                              .eUART(eUART), .eDisp(eDisp) );
                              
    PC_Processor_2 PC_Processor (
        .OP1(outA),       //operando 1
        .OP2(outB),       //operando 2
        .OpCode(opCode),    //codigo de operacion
        .clk(clk), .rst(rst),// .op_start(op_start),
        .result(result_out)
    );
    
//     ila_0 ILA (
//         .clk(clk),
//         .probe0(op_start),
//         .probe1(result_out)
//     );
    
    
endmodule

module PC_Controller_2  #(parameter word='d8, parameter num_word=1024)(
    input  logic           clk, rst,
           logic           tx_ready, op_start,
           logic [2:0]     opCode,
    output logic           eUART, eDisp
    );
    
    // FALTA TODO EL MODULO
    typedef enum logic [1:0] {IDLE, OPERATING, TX_START} state;
    state pr_state, nx_state;
    
    logic [3:0] count = 'd0;
    logic [3:0] count_temp = 'd0;
    logic [3:0] cicle_delay = ('d3 + $clog2(num_word)); 
    
    //FSM state register:
    always_ff @(posedge clk) begin
	   if (rst) begin
	       pr_state <= IDLE;
	       count <= 0;
	   end
	   else begin
	       count <= count_temp;
	       pr_state <= nx_state;
	   end
    end
    
    always_comb begin
        eUART = 'b0;
        eDisp = 'b0;
        nx_state = IDLE;
        count_temp = count;
        
        case(pr_state)
            IDLE: begin
                if( op_start ) nx_state = OPERATING;
                else nx_state = IDLE;
                count_temp = 'd0;
            end
            
            OPERATING: begin
                if ( (opCode <= 3'b011)&&(count > 'd3) ) begin
                    nx_state = TX_START;
                end else if ( ( (opCode == 3'b100)||(opCode == 3'b101)  )&&(count > cicle_delay))  begin 
                    nx_state = TX_START;
                end else begin
                    nx_state = OPERATING;
                    count_temp = count +1;
                end 
//                if (count < cicle_delay) begin
//                    nx_state = OPERATING;
//                    count_temp = count +1;
//                end else begin
//                    nx_state = TX_START;
//                end
            end
            
            TX_START: begin
                
                //decir cuando transmitir
                if (opCode <= 3'b011) begin
                    eUART = 1;
                end else if( (opCode == 3'b100)||(opCode == 3'b101)  ) begin 
                    eUART = 1;
                    eDisp = 1;
                end
                if (tx_ready) nx_state = IDLE;
                else nx_state = TX_START;
            end
            default: begin
                nx_state = IDLE;
            end        
        endcase
                
    end
    
    

endmodule 

module PC_Processor_2 #(parameter word='d8, parameter num_word=1024) (
    input  logic [num_word*word-1:0] OP1,       //operando 1
           logic [num_word*word-1:0] OP2,       //operando 2
           logic [2:0]   OpCode,    //codigo de operacion
           logic          clk, rst,// op_start,
    output logic [num_word*word-1:0] result
);

 logic [num_word*word-1:0] result_temp;
 logic [3*word*(num_word-1)-1:0] suma_aux;
 logic [num_word*(word+1)-1:0] sum;
 
 var int num_sums = num_word/2;
 var int prev_index = 0;
 var int curr_index = num_sums;
 
 
generate 
  
always_comb begin
    result = 'd0;
    case(OpCode)
        3'b000:  result = OP1;                                     //readVec BRAMA
        3'b001:  result = OP2;                                     //readvec BRAMB
        3'b010:  begin                                              //sumVec
            for (int i=0; i<num_word; i++) begin
                result[i*word+:word] = sum[i*(word+1)+:word] ;
            end
        end                           
        3'b011:   begin                                            // avgVec
            for (int i=0; i<num_word; i++) begin
                result[i*word+:word] = sum[i*(word+1)+:word]/2 ;
            end
        end
        3'b101: begin
            result = 'd0;
            result[3*word:0] = suma_aux[3*word*(num_word-1)-1:3*word*(num_word-1)-3*word];
        end
        default: begin
            result = 'd0;
        end
    endcase    
end


always_ff @(posedge clk) begin
    if (rst) begin
        suma_aux <= 'd0;
        result_temp <= 'd0;
        sum <= 'd0;
    end else begin
    
     for (int i=0; i<num_word; i++) begin
        sum[i*(word+1)+:(word+1)] = OP1[i*word+:word] + OP2[i*word+:word];
     end
    
    for (int i=0; i<num_word; i++) begin
       if ( OP1[i*word+:word] > OP2[i*word+:word] )
                result_temp[i*word+:word] <=  OP1[i*word+:word] - OP2[i*word+:word] ;
            else
                result_temp[i*word+:word] <=  OP2[i*word+:word] - OP1[i*word+:word] ;
        end
        // creacion del arbol con pipeline
        num_sums = num_word/2;
        for (int i=0 ; i <num_sums ; i++) begin
            suma_aux[i*word*3+:word*3] <= result_temp[2*i*word+:word] + result_temp[(2*i+1)*word+:word];   
        end
        
        prev_index = 0;
        curr_index = num_sums;
        num_sums = num_sums/2;
        
        for (int i=1 ; i < $clog2(num_word) ; i++ ) begin
             for (int j=0 ; j<num_sums ; j++) begin
                 suma_aux[(j+curr_index)*word*3+:word*3] <= suma_aux[(2*j+prev_index)*word*3+:word*3] + suma_aux[(2*j+1+prev_index)*word*3+:word*3] ;  
             end
             prev_index = curr_index;
             curr_index = curr_index + num_sums;
             num_sums = num_sums/2;
        end 
    end
end 
    
endgenerate 


//ila_0 ILA(
//    .clk(clk),
//    .probe0(OpCode),
//    .probe1(suma_aux[127:0]),
//    .probe2(suma_aux[191:128]),
//    .probe3(suma_aux[223:192])
//);


endmodule 