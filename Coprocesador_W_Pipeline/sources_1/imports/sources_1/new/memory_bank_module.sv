`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 22.12.2022 17:03:27
// Design Name: 
// Module Name: memory_bank_module
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module memory_bank_module #( parameter word=8, num_word=1024)(
    input   logic clk_in,  rst, wea,
            logic [9:0]  address,
            logic [word-1:0]  data_in,
    output  logic [word*num_word -1:0]    data_out
    );
    
    logic [num_word-1:0] write_flag = 'd0;
    
    always_comb  begin
        write_flag = 'd0;
        write_flag[address] = wea;
    end
   
    for( genvar  i=0 ; i<num_word ; i = i + 1)
        begin
            FDCE_module #(word) FCDE  ( .clk_in(clk_in), .rst(rst), .write_flag(write_flag[i]), .data_in(data_in), .data_out(data_out[word*(i+1)-1:word*i]) );
        end
    
    
endmodule