# Tarea 3:

Autores:
-      Gustavo   Silva              201721012-5            gustavo.silvasi@sansano.usm.cl        
-      Benjamin  Villegas           201721007-9            benjamin.villegas@sansano.usm.cl  

En este se presenta el desarrollo de la Tarea 3 del curso Sistemas Digitales Avanzados. Ésta busca desarrollar capacidades recordadas en la tarea previa, y añadir análisis de paralelismo en circuitos digitales con FPGA. Para ello es que se abordan elementos de análisis de tiempo, esquemas de diseño, máquinas de estado, GPU, coprocesadores, etc. en la implementación de un coprocesador para operación sobre vectores.

El desarrollo actualizado se encuentra en el siguiente repositorio: https://gitlab.com/gustavo.silvasi/ipd432_t3_2022

En esta se desarrolan 2 diseños, el Con Pipeline presente en la carpeta "Coprocesador_W_Pipeline" , y el Sin Pipeline presente en la carpeta "Coprocesador_WO_Pipeline".

Además, es importante tener en cuenta la version anterior de este trabajo, la Tarea 2, presente en el siguiente repositorio: https://gitlab.com/gustavo.silvasi/coprocesador

El diseño y proceso se encuentra explicado en el informe titulado por "IPD432_Tarea_3.pdf".