`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11.09.2022 16:20:13
// Design Name: 
// Module Name: change_display
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module change_display
 (  input logic [2:0] counter ,
    input logic [6:0] simbol_1,
    input logic [6:0] simbol_2,
    input logic [6:0] simbol_3,
    input logic [6:0] simbol_4,
    input logic [6:0] simbol_5,
    input logic [6:0] simbol_6,
    input logic [6:0] simbol_7,
    input logic [6:0] simbol_8,
    output logic [7:0] disp,
    output logic [6:0] word_show );
 
 always_comb begin
    case ( counter )
        3'b000:
            begin
            disp <= ~8'b00000001;
            word_show <= simbol_1;
            end
        3'b001:
            begin
            disp <= ~8'b00000010;
            word_show <= simbol_2;
            end
        3'b010:
            begin
            disp <= ~8'b00000100;
            word_show <= simbol_3;
            end
        3'b011:
            begin
            disp <= ~8'b00001000;
            word_show <= simbol_4;
            end
        3'b100:
            begin
            disp <= ~8'b00010000;
            word_show <= simbol_5;
            end
        3'b101:
            begin
            disp <= ~8'b00100000;
            word_show <= simbol_6;
            end
        3'b110:
            begin
            disp <= ~8'b01000000;
            word_show <= simbol_7;
            end
        3'b111:
            begin
            disp <= ~8'b10000000;
            word_show <= simbol_8;
            end
        default:
            begin
            disp <= ~8'b00000001;
            word_show <= ~8'b00000001;
            end
    endcase
 end 
endmodule
