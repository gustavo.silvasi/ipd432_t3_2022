`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 12.10.2022 22:50:35
// Design Name: 
// Module Name: module_display
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module module_display(
    input logic CLK100MHZ, //reloj
          logic eDisp,
          logic weCMD,
          logic rst,
          logic [31:0] result, //valores de entrada al display
    output logic [7:0] AN, //que display estoy
           logic [6:0] SEG //segmentos del display
    );
    
    logic c1k_60; //reloj de 60hz
    logic [2:0] count = 3'b000; //contador para saber en que display estoy
    logic [6:0] word_disp_1, word_disp_2, word_disp_3, word_disp_4,  //simbolo a escribir en cada display
                word_disp_5, word_disp_6, word_disp_7, word_disp_8;
    logic state;
    
    clock_divider #(480) divider60(.clk_in(CLK100MHZ),.clk_out(c1k_60)); //480 es por 60*8 display
    // formar un reloj de 480Hz debe entrar el doble de la frec deseada es decir 960
    
    //cosas del display
    logic [31:0] rBCD;
    
    unsigned_to_bcd double_dabble(
        .clk        (CLK100MHZ),     //revisar
        .trigger    (eDisp),
        .in         (result),
        .idle       (),
        .bcd        (rBCD)
    );
    
    BCD bcd1(.clk(c1k_60), .BCD_in(rBCD[31:28]),   .state(state), .sevenSeg(word_disp_8) );
    BCD bcd2(.clk(c1k_60), .BCD_in(rBCD[27:24]),   .state(state), .sevenSeg(word_disp_7) );
    BCD bcd3(.clk(c1k_60), .BCD_in(rBCD[23:20]),  .state(state), .sevenSeg(word_disp_6) );
    BCD bcd4(.clk(c1k_60), .BCD_in(rBCD[19:16]), .state(state), .sevenSeg(word_disp_5) );
    BCD bcd5(.clk(c1k_60), .BCD_in(rBCD[15:12]), .state(state), .sevenSeg(word_disp_4) );
    BCD bcd6(.clk(c1k_60), .BCD_in(rBCD[11:8]), .state(state), .sevenSeg(word_disp_3) );
    BCD bcd7(.clk(c1k_60), .BCD_in(rBCD[7:4]), .state(state), .sevenSeg(word_disp_2) );
    BCD bcd8(.clk(c1k_60), .BCD_in(rBCD[3:0]), .state(state), .sevenSeg(word_disp_1) );
    
    change_display  cambio_disp(    
        .counter    (count) ,
        .simbol_1   ( word_disp_1 ),
        .simbol_2   ( word_disp_2 ),
        .simbol_3   ( word_disp_3 ),
        .simbol_4   ( word_disp_4 ),
        .simbol_5   ( word_disp_5 ),
        .simbol_6   ( word_disp_6 ),
        .simbol_7   ( word_disp_7 ),
        .simbol_8   ( word_disp_8 ),
        .disp       ( AN ), 
        .word_show  ( SEG )
    );
    
    always @(posedge c1k_60) begin
        if (count == 3'b111) begin
            count <= 3'b000;
        end else begin
            count <= count + 3'b001;
        end
        
    end
    
    always_ff @(posedge CLK100MHZ) begin
        if( rst )
            state = 'b0;
        else if ( eDisp )
            state = 'b1;
        else if (weCMD)
            state = 'b0;
        else
            state = state;    
    end
    
    
endmodule
