`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 22.11.2022 09:40:25
// Design Name: 
// Module Name: uart_rx_logic
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module uart_rx_logic(
    input  logic           clk_100M,
           logic           reset,
           logic           rx_ready,
           logic [7:0]     rx_data,
    output logic [7*8-1:0] cmd_string,
           logic [5*8-1:0] flag_string,
           logic [7:0]     num,
           logic [9:0]     address,
           logic           weA, weB, weCMD
    );
    
    logic [3:0] dmx_ctrl;
    
    module_rx_ctrl uart_rx_fsm(
        .rx_ready(rx_ready),
        .clk(clk_100M),
        .rst(reset),        
        .cmd(cmd_string),
        .flag(flag_string),
        .dmx_ctrl(dmx_ctrl),
        .address(address),
        .weA(weA),
        .weB(weB),
        .weCMD(weCMD)
    );
    
    uart_rx_demux rx_demux(
        .clk(clk_100M),
        .rst(reset),
        .dmx_ctrl(dmx_ctrl),
        .rx_data(rx_data),
        .cmd_string(cmd_string),
        .flag_string(flag_string),
        .num(num)
    );
    
    /**Posiilidad de que weCMD se mande en IDLE cosa que ahi si estar� listo*/
    
endmodule
