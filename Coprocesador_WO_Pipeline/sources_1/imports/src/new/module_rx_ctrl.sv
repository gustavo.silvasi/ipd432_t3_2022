`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 17.11.2022 23:33:21
// Design Name: 
// Module Name: module_rx_ctrl
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module module_rx_ctrl
    #( parameter cmd_value='d7, parameter flag_cmd_value='d5, parameter num_value='d1024 )
    (
    input  logic rx_ready, clk, rst,
           logic [7*8-1:0] cmd,
           logic [5*8-1:0] flag,
    output logic [3:0] dmx_ctrl,
           logic [9:0] address,
           logic weA, weB, weCMD
    );
    
    //Declarations:------------------------------

 logic [9:0] address_next;
  
 //FSM states type:
 enum logic [3:0] {IDLE, WAIT_CMD, SAVE_CMD, DELAY_CMD, TRIGGER_CMD,
                            WAIT_FLAG , SAVE_FLAG , DELAY_FLAG, TRIGGER_FLAG, 
                            WAIT_NUM, SAVE_NUM, DELAY_NUM, TRIGGER_NUM} pr_state, nx_state;

  //Timer-related declarations:
 const logic [8:0] T1 = 7 ;
 const logic [8:0] T2 = 5 ;
 const logic [10:0] T3 = 1024;
 //const logic [10:0] T3 = 8;
 //const logic [8:0] tmax = 1024 ;//tmax ? max(T1,T2,...)-1
 logic [10:0] t;


 //Part 3: Statements:---------------------------------------

 //Timer :
 always_ff @(posedge clk) begin
	if (rst) t <= 'b0;
	else if ( ( pr_state == DELAY_CMD ) || ( pr_state == DELAY_FLAG ) || ( pr_state == TRIGGER_NUM  ) ) t <= 0;
	else if ( ( pr_state == SAVE_CMD )  || ( pr_state == SAVE_FLAG )  || ( pr_state == DELAY_NUM   ) ) t <= t + 1;
end

 //FSM state register:
 always_ff @(posedge clk) begin
	if (rst) begin
	   pr_state <= IDLE;
	   address <= 'd0;
	end
	else begin
	   pr_state <= nx_state;
	   address <= address_next;
	end
 end
 //FSM combinational logic:
 always_comb begin
 
    dmx_ctrl = 'b0;
    address_next  = address;
    weA      = 'b0;
    weB      = 'b0;
    weCMD    = 'b0;
    nx_state = IDLE;
    
	case (pr_state)	    
		IDLE: begin
	       nx_state = WAIT_CMD;
	       //weCMD = (cmd != "WRITE  "); 
	       address_next = 'd0;
	    end
 
		WAIT_CMD: nx_state = (rx_ready)?(SAVE_CMD):(WAIT_CMD);
		
		SAVE_CMD: begin
		      dmx_ctrl = t+'d1;
		      nx_state = (t >= T1-1)?(DELAY_CMD):(WAIT_CMD);
		end
        
        DELAY_CMD: begin
            nx_state = TRIGGER_CMD;
            if((cmd != "readVec")&&(cmd != "WRITE  "))
                weCMD = 'b1;
        end 
        TRIGGER_CMD: begin 
                if((cmd == "readVec")||(cmd == "WRITE  ")) nx_state = WAIT_FLAG;
                else begin
                    nx_state = IDLE;
                    weCMD = 'b1;
                end
        end	      
        
        WAIT_FLAG: nx_state = (rx_ready)?(SAVE_FLAG):(WAIT_FLAG);
        
        SAVE_FLAG: begin
		      dmx_ctrl = t+'d7+'d1;
		      nx_state = (t >= T2-1)?(DELAY_FLAG):(WAIT_FLAG);
		end
		
		DELAY_FLAG: begin
		  nx_state = TRIGGER_FLAG;
		  if ( cmd != "WRITE  ")
                weCMD = 'b1;
        end
		
		TRIGGER_FLAG: begin
		      if(cmd == "WRITE  ") nx_state = WAIT_NUM;
		      else begin
		          nx_state = IDLE;
		          weCMD = 'b1;
		      end		      
		end
		
		WAIT_NUM: begin
		  dmx_ctrl = 'd5+'d7+'d1;
		  if(rx_ready) begin
		      nx_state = SAVE_NUM;
		  end
		  else nx_state = WAIT_NUM;
		end
		
		SAVE_NUM: begin
		      //address_next = address + 1;
		      dmx_ctrl = 'd5+'d7+'d1;
		      if(flag == "BRAMA") weA = 'b1;
		      else if(flag == "BRAMB") weB  = 'b1;
		      nx_state = DELAY_NUM;
		end
		
		DELAY_NUM: begin
		  //nx_state = TRIGGER_NUM;
		  dmx_ctrl = 'd5+'d7+'d1;
		  address_next = address + 1;
		  nx_state = (t >= T3-1)?(TRIGGER_NUM):(WAIT_NUM);
		end
		
		TRIGGER_NUM: nx_state = IDLE;
		
		default: begin
		   nx_state = IDLE;
	       weCMD = 'd0; 
	       address_next = 'd0;
	    end   
			
	endcase
end

/**
        B: begin
			outp1 = <value> ;
			outp2 = <value> ;
			...
			if (... and t >= T3-1) nx_state = C;
			else if (...) nx_state = ...;
			else nx_state = B;
		end
*/
/**
 //Optional output register (if required). Adds a FF at the output to prevent the propagation of glitches from comb. logic.
	always_ff @(posedge clk)
		if (rst) begin //rst might be not needed here
			new_outp1 <= ...;
			new_outp2 <= ...; ...
		end
		else begin
			new_outp1 <= outp1;
			new_outp2 <= outp2; ...
		end
*/   
endmodule
