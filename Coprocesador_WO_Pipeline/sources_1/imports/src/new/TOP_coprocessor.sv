`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 17.11.2022 15:46:57
// Design Name: 
// Module Name: TOP_coprocessor
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module TOP_coprocessor(
    input logic CLK100MHZ, //reloj
    input logic CPU_RESETN, //reset
    input logic UART_TXD_IN,
    output logic [7:0] AN, //que display estoy
    output logic [6:0] SEG, //segmentos del display
    output logic DP,
    output logic UART_TXD
    );
          
    logic rst;
    assign rst = ~CPU_RESETN;
    
    assign DP = 'd1;
    
    logic clk_50M;
    
    clk_wiz_0 clk_wiz
   (
    // Clock out ports
    .clk_50MHZ(clk_50M),     // output clk_50MHZ
    // Status and control signals
    .reset(rst), // input reset
    .locked(),       // output locked
   // Clock in ports
    .clk_in1(CLK100MHZ));      // input clk_in1
        
    logic [7:0] rx_data, tx_data;
    logic rx_ready, tx_start, tx_busy;
    
    /* Modulo UART a 115200/8 bits datos/No paridad/1 bit stop */
	uart_basic #(
		//.CLK_FREQUENCY(100000000), // reloj base de entrada
		.CLK_FREQUENCY(35_000_000), // reloj base de entrada
		.BAUD_RATE(115200)
	) uart_basic_inst (
		.clk(clk_50M),
		.reset(rst),
		.rx(UART_TXD_IN),
		.rx_data(rx_data),
		.rx_ready(rx_ready),
		.tx(UART_TXD),
		.tx_start(tx_start),
		.tx_data(tx_data),
		.tx_busy(tx_busy)
	);
	
	logic[7:0] inA, inB, num;
	logic [8*1024-1:0]  outA, outB;
	//logic [8*8-1:0]  outA, outB;
    logic[9:0] addr_inA, addr_inB, addr_outA,addr_outB, address;
    logic weA, weB;
    logic[7*8-1:0] cmd;
    logic[5*8-1:0] flag;
	
	uart_rx_logic rx_logic(
	   .clk_100M(clk_50M),
	   .reset(rst),
	   .rx_ready(rx_ready),
	   .rx_data(rx_data),
	   .cmd_string(cmd),
	   .flag_string(flag),
	   .num(num),
	   .address(address),
	   .weA(weA),.weB(weB),.weCMD(weCMD)
	); 
        
    assign addr_inA = address;
    assign addr_inB = address;
    assign inA = num;
    assign inB = num; 
    

    memory_bank_module BRAMA (
        .clk_in(clk_50M),
        .rst(rst),
        .wea(weA),
        .address(addr_inA),
        .data_in(inA),
        .data_out(outA)
    );

    memory_bank_module BRAMB (
        .clk_in(clk_50M),
        .rst(rst),
        .wea(weB),
        .address(addr_inB),
        .data_in(inB),
        .data_out(outB)
    );
    
    
    logic [2:0] opCode;
    
    command_decoder decoder(
        .clk(clk_50M),
        .rst(rst),
        .command_string(cmd),
        .flag_string(flag),
        .weCMD(weCMD),
        .opCode(opCode)        
    );
    
    logic [7:0] result;
    logic eUART, eDisp;
    logic tx_ready; 
    logic [1024*8-1:0] aux_result; 
    //logic [8*8-1:0] aux_result; 

    Processing_Core_2  Processing_Core (
        .outA(outA),
        .outB(outB),
        .opCode(opCode),
        .clk(clk_50M), .rst(rst),
        .tx_ready(tx_ready),
        .op_start(weCMD), 
        .result_out(aux_result),
        .eUART(eUART), .eDisp(eDisp) 
    );
    
    logic [9:0] address_read;
    logic [7:0] result_read;
    
    memory_bank_MISO_Module memory_out (
    .clk_in(clk_50M),  .rst(rst),
    .address_read(address_read),
    .data_in(aux_result),
    .data_out(result_read)
    );
    
    TX_CTRL_TOP_module TX_CTRL_TOP(
    .clk(clk_50M), .rst(rst),
    .ps_ready(eUART ), // BANCO DE MEMORIA DE SALIDA LISTO
    .opCode(opCode),
    .result(result_read), 
    .tx_start(tx_start),
    .tx_ready(tx_ready),
    .tx_data(tx_data),
    .adress_read(address_read)         
    );
    
    module_display display(
        .CLK100MHZ  (clk_50M),    //quizas meter un clk de display por wizard
        .eDisp      (eDisp), // eDisp
        .weCMD      (weCMD),
        .rst        (rst),
        .result     (aux_result[31:0]), // iba result
        .AN         (AN),
        .SEG        (SEG)
    );
    
//     ila_0 ILA (
//     .clk(clk_50M),
//     .probe0(weCMD),
//     .probe1(eDisp)
////     .probe2(eDisp),
////     .probe3(rst)
//     );
    
    
endmodule
