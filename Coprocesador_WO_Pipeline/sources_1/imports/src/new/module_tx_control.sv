`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 23.11.2022 12:04:36
// Design Name: 
// Module Name: module_tx_control
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module module_tx_control
#(	parameter INTER_BYTE_DELAY = 10000,   // 1000000 ciclos de reloj de espera entre el envio de 2 bytes consecutivos
	parameter WAIT_FOR_REGISTER_DELAY = 100, // tiempo de espera para iniciar la transmision luego de registrar el dato a enviar
	parameter NUM_OF_BYTES = 4
)(
    input logic clk, rst,
          logic eUART,
          logic [2:0] opCode,
          logic [31:0] result, 
    output logic tx_start,
           logic tx_ready,
           logic [7:0] tx_data          
    );
    
    //FSM states type:
 enum logic [1:0] {IDLE, READ, SEND, DELAY} pr_state, nx_state;
 
 logic [19:0] t;
 logic [1:0] c, c_next;
 
  //Part 3: Statements:---------------------------------------

 //Timer :
 always_ff @(posedge clk) begin
	if (rst) t <= 0;
	else if (pr_state != nx_state) t <= 0; //reset the timer when state changes
	else if (t != INTER_BYTE_DELAY) t <= t + 1;
 end
 
 //FSM state register:
 always_ff @(posedge clk) begin
	if (rst) begin
	   pr_state <= IDLE;
	   c <= 0;
	end
	else begin
	   pr_state <= nx_state;
	   c <= c_next;
	end
 end
 
 always_comb begin
    tx_start = 'b0;
    tx_ready = 'b1;
    c_next = c;
    tx_data = result[7:0];
    nx_state = IDLE;
    case(pr_state)
        IDLE: begin
            if(eUART) nx_state = READ;
            c_next = 0;
        end
        READ: begin
            tx_ready = 'b0;
            if (t >= WAIT_FOR_REGISTER_DELAY-1) nx_state = SEND;
			else nx_state = READ;
	   end
	   SEND: begin
	       tx_ready = 'b0;
	       tx_start = 'b1;
	       nx_state = DELAY;
	   end
	   DELAY: begin
	       tx_ready = 'b0;
	       if(((opCode <= 3'b011)||(c >= NUM_OF_BYTES-1))&&(t >= INTER_BYTE_DELAY-1)) nx_state = IDLE;
	       else if(((opCode == 3'b100)||(opCode == 3'b101))&&(t >= INTER_BYTE_DELAY-1)&&(c < NUM_OF_BYTES-1))begin
	           nx_state = READ;
	           c_next = c + 1;
	       end
           else nx_state = DELAY;
	   end        
    endcase
    
    if(c == 'd0) tx_data = result[7:0];
    if(c == 'd1) tx_data = result[15:8];
    if(c == 'd2) tx_data = result[23:16];
    if(c == 'd3) tx_data = result[31:24];
    
 end
 
    
endmodule
