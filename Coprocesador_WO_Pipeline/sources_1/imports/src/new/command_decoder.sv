`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 22.11.2022 08:49:51
// Design Name: 
// Module Name: command_decoder
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module command_decoder(
    input logic clk, rst,
          logic [7*8-1:0] command_string,
          logic [5*8-1:0] flag_string,
          logic weCMD,
    output logic [2:0] opCode
    );
    
    logic [2:0] opCode_next;
    
    always_comb begin
        opCode_next = 3'b111;
        case(command_string)
            "readVec": begin
                opCode_next = 3'b111;
                if(flag_string == "BRAMA") opCode_next = 3'b000;
                else if(flag_string == "BRAMB") opCode_next = 3'b001;
            end
            "sumVec ":  opCode_next = 3'b010;
            "avgVec ":  opCode_next = 3'b011;
            "eucDist": opCode_next = 3'b100;
            "manDist": opCode_next = 3'b101;
            default:   opCode_next = 3'b111; 
        endcase         
    end
    
    always_ff @(posedge clk) begin
        if (rst) opCode <= 3'b111;
        else if(weCMD) opCode <= opCode_next;
        else opCode <= opCode;
	   
    end
    
    
    
endmodule
