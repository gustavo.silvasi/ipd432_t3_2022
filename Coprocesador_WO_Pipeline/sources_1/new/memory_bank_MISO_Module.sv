`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 23.12.2022 15:55:27
// Design Name: 
// Module Name: memory_bank_MISO_Module
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module memory_bank_MISO_Module #( parameter word=8, num_word=1024)(
    input   logic clk_in,  rst,
            logic [9:0] address_read,
            logic [word*num_word -1:0]  data_in,
    output  logic [word-1:0]    data_out
    );
     
    logic [1024*8-1:0] aux_out;
    
    for( genvar  i=0 ; i<num_word ; i = i + 1)
        begin
            FDCE_module #(word) FCDE  ( .clk_in(clk_in), .rst(rst), .write_flag('b1), .data_in(data_in[word*(i+1)-1:word*i]), .data_out(aux_out[word*(i+1)-1:word*i]) );
        end
        
    always_comb begin
        data_out = 'd0;
        for (int i=0; i<num_word; i++)
            if (i == address_read) data_out = aux_out[i*word+:word];
    end  
endmodule
