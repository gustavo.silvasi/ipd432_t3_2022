`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 22.12.2022 19:28:31
// Design Name: 
// Module Name: TX_CTRL_TOP_module
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module TX_CTRL_TOP_module(
    input logic clk, rst,
          logic ps_ready, // BANCO DE MEMORIA DE SALIDA LISTO
          logic [2:0] opCode,
          logic [7:0] result, 
    output logic tx_start,
           logic tx_ready,
           logic [7:0] tx_data,
           logic [9:0] adress_read         
    );
    
 //FSM states type:
 enum logic [2:0] {IDLE, START, STOP, ADD_ADDRESS, DELAY} pr_state, nx_state;
 
logic [10:0] c, c_next;
logic eUART;
 
  //Part 3: Statements:---------------------------------------


 //FSM state register:
 always_ff @(posedge clk) begin
	if (rst) begin
	   pr_state <= IDLE;
	   c <= 0;
	end
	else begin
	   pr_state <= nx_state;
	   c <= c_next;
	end
 end
 
 always_comb begin

    eUART = 'b0;
    c_next = c;
    nx_state = IDLE;
    case(pr_state)
        IDLE: begin
            if(ps_ready) nx_state = START;
            c_next = 0;
        end
        START: begin
            eUART  = 'b1;
            nx_state = STOP;
	   end
	   STOP:
	       begin
	       nx_state = STOP ;
	       if(tx_ready) nx_state = ADD_ADDRESS ;
	       end
	   ADD_ADDRESS: begin
	       c_next = c  +1;
	       nx_state = DELAY ;
	   end
	   DELAY: begin
	       //if ( ( opCode <= 3'b011) && (c > 1024 ) ) nx_state = IDLE;
	       if ( ( opCode <= 3'b011) && (c > 1024 ) ) nx_state = IDLE;
	       else if ( ( (opCode == 3'b100)||(opCode == 3'b101) ) && (c > 4 ) ) nx_state = IDLE;
           else nx_state = START ;
	   end        
    endcase
    
   
 end
    
 assign adress_read = c;
    
 module_tx_control tx_logic(
        .clk(clk),
        .rst(rst),
        .eUART(eUART),
        .opCode(3'b000),
        .result(result), 
        .tx_start(tx_start),
        .tx_ready(tx_ready),
        .tx_data(tx_data)
    );
    
endmodule
