`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 23.12.2022 16:05:19
// Design Name: 
// Module Name: Processing_Core_2
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Processing_Core_2 #(parameter word='d8, parameter num_word=1024) (
    input  logic [num_word*word-1:0]   outA,
           logic [num_word*word-1:0]   outB,
           logic [2:0]     opCode,
           logic           clk, rst,
           logic           tx_ready,
           logic           op_start, 
    output logic [num_word*word-1:0]   result_out,
           logic eUART, eDisp 
    );
    
    // FALTA TODO EL MODULO
    PC_Controller_2 PC_ctrl ( .clk(clk), .rst(rst), .tx_ready(tx_ready) ,
                              .op_start(op_start), .opCode(opCode) ,
                              .eUART(eUART), .eDisp(eDisp) );
                              
    PC_Processor_2 PC_Processor (
        .OP1(outA),       //operando 1
        .OP2(outB),       //operando 2
        .OpCode(opCode),    //codigo de operacion
        //.clk(clk), //.rst(rst), .op_start(op_start),
        .result(result_out)
    );
    
//     ila_0 ILA (
//         .clk(clk),
//         .probe0(op_start),
//         .probe1(result_out)
//     );
    
    
endmodule

module PC_Controller_2 (
    input  logic           clk, rst,
           logic           tx_ready, op_start,
           logic [2:0]     opCode,
    output logic           eUART, eDisp
    );
    
    // FALTA TODO EL MODULO
    typedef enum logic [2:0] {IDLE, TX_START} state;
    state pr_state, nx_state;
    
    //FSM state register:
    always_ff @(posedge clk) begin
	   if (rst) begin
	       pr_state <= IDLE;
	   end
	   else begin
	       pr_state <= nx_state;
	   end
    end
    
    always_comb begin
        eUART = 'b0;
        eDisp = 'b0;
        nx_state = IDLE;
        
//        if( (opCode == 3'b100)||(opCode == 3'b101)  )
//            eDisp = 'b1;
//        else
//            eDisp = 'b0;
        
        case(pr_state)
            IDLE: begin
                if( op_start ) nx_state = TX_START;
                else nx_state = IDLE;
                
            end
            
            TX_START: begin
                
                //decir cuando transmitir
                eUART = 1;
                if (opCode <= 3'b011) begin
                    eUART = 1;
                end else begin//if( (opCode == 3'b100)||(opCode == 3'b101)  ) begin 
                    eUART = 1;
                    eDisp = 1;
                end
                if (tx_ready) nx_state = IDLE;
                else nx_state = TX_START;
            end
            default: begin
                nx_state = IDLE;
            end        
        endcase
                
    end
    
    

endmodule 

module PC_Processor_2 #(parameter word='d8, parameter num_word=1024) (
    input  logic [num_word*word-1:0] OP1,       //operando 1
           logic [num_word*word-1:0] OP2,       //operando 2
           logic [2:0]   OpCode,    //codigo de operacion
           //logic          clk,// rst, op_start,
    output logic [num_word*word-1:0] result
);

 logic [num_word*word-1:0] result_temp = 'd0;
 logic [9:0] num_sums = num_word/2;
 logic [4*word*num_word-1:0] suma_aux [0:$clog2(num_word)-1] ;
 
 
 always_comb begin
        result = 'b0;
        result_temp = 'd0;
        for (int a= 0 ; a < $clog2(num_word) ; a++)
            suma_aux[a] = 'd0;
        case(OpCode)
            3'b000:  result = OP1;                                     //readVec BRAMA
            3'b001:  result = OP2;                                     //readvec BRAMB
            3'b010:  begin                                //sumVec
                        for (int i=0; i<num_word; i++)
                            result[i*word+:word] = OP1[i*word+:word] + OP2[i*word+:word];
                     end
            3'b011:   begin                                // avgVEC
                        for (int i=0; i<num_word; i++)
                            result[i*word+:word] = ( OP1[i*word+:word] + OP2[i*word+:word] )/2 ;
                     end
            //3'b100:  result_temp = (OP1 - OP2)*(OP1-OP2) + OP3;             //eucDist
            3'b101:  begin //manDist
                // se calculan distancias
                for (int i=0; i<num_word; i++) begin
                    if ( OP1[i*word+:word] > OP2[i*word+:word] )
                        result_temp[i*word+:word] =  OP1[i*word+:word] - OP2[i*word+:word] ;
                    else
                        result_temp[i*word+:word] =  OP2[i*word+:word] - OP1[i*word+:word] ;
                end
                // se crea �rbol 
                
               for (int i=0 ; i <num_sums ; i++) begin
                   suma_aux[0][i*word*4+:word*4] = result_temp[2*i*word+:word] + result_temp[(2*i+1)*word+:word];   
               end
               for (int i=1 ; i < $clog2(num_word) ; i++ ) begin
                    for (int j=0 ; j<num_word/(2**i) ; j++) begin
                        suma_aux[i][j*word*4+:word*4] = suma_aux[i-1][2*j*word*4+:word*4] + suma_aux[i-1][(2*j+1)*word*4+:word*4] ; 
                    end
               end               
               
               //result = 'd0;
               result[4*word:0] = suma_aux[$clog2(num_word)-1][4*word:0];
                
                
            end
                
            default: result = 'd0;                                     //readVec BRAMA
        endcase
  end
  

endmodule 