`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 22.12.2022 17:13:28
// Design Name: 
// Module Name: FDCE_module
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module FDCE_module #( parameter word=8)(
    input   logic clk_in, rst, write_flag,
            logic [word-1:0]    data_in,
    output  logic [word-1:0]    data_out
    );
    
    logic [word-1:0] aux;
    
    always_ff @(posedge clk_in) begin
       if (rst) begin
           data_out <= 'b0;
       end else begin
           data_out <= aux;
       end
    end
    
    always_comb begin
        aux = data_out;
        if (write_flag == 'b1)
            aux = data_in;
    end
    
endmodule
